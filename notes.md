# EMBL Rome Bioinformatics Services

This website is powered by Jekyll and some Bootstrap, Bootwatch.

The template of this site was forked and modified from the <a href="https://github.com/mpa139/allanlab"> Allan Lab Website </a>. We truly thanks them for making their code available.

Go to their *aboutwebsite.md*  to learn how to copy and modidy their page for your purpose.

If you are new to Jekyll, we recommend this <a href="https://youtu.be/T1itpPvFWHI"> tutorial </a> by Giraffe Academy.



