---
title: "EMBL Rome Bioinformatics Services - Home"
layout: homelay
excerpt: "EMBL Rome Bioinformatics Services."
sitemap: false
permalink: /
---

You will find on this website different resources about Bioinformatics at EMBL Rome. The services section describes the type of analysis that you can get from the platform with output showcases. You can contact us at bioinfosupport@embl.it. The training section provides different resources spanning a broad range of subjects.


<div markdown="0" id="carousel" class="carousel slide" data-ride="carousel" data-interval="5000" data-pause="hover" >
    <!-- Menu -->
    <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
        <li data-target="#carousel" data-slide-to="3"></li>
        <li data-target="#carousel" data-slide-to="4"></li>
    </ol>

    <!-- Items -->
    <div class="carousel-inner" markdown="0">

        <div class="item active">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/chr6-69951864-81654911_overview.png" alt="Slide 1" />
        </div>
        <div class="item">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/wordcloud.png" alt="Slide 2" />
        </div>
        <div class="item">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/chr6-69951864-81654911_overview.png" alt="Slide 3" />
        </div>
        <div class="item">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/chr6-69951864-81654911_overview.png" alt="Slide 4" />
        </div>
        <div class="item">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/chr6-69951864-81654911_overview.png" alt="Slide 5" />
        </div>
        <div class="item">
            <img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/chr6-69951864-81654911_overview.png" alt="Slide 6" />
        </div>
    </div>
  <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


The Bioinformatics Services provide analysis and computational expertise to the institute. The main aim is to advance research in a collaborative environment. Tasks include training, consultancy and bioinformatics analysis. One goal is to facilitate quantitative approaches while ultimately enriching the scientists’ research agenda.

For example, high volume data like sequencing data require an adequate experimental design, a tailored data processing pipeline and statistics. Analyses are primarily based on open source software. This fosters exchange and enables further research to be carried out independently.

Computing resources are expanding on site, in addition, links and access to high performance computing at EMBL Heidelberg are available. The Services are continuously evolving according to the needs of the research projects.
